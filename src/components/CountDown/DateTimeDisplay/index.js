import React from "react";
import "./DataTimeDisplay.css";

const DateTimeDisplay = ({ value, type }) => {
  return (
    <div className="countdown">
      <p className="countdown_time">{value}</p>
      <span className="type">{type}</span>
    </div>
  );
};

export default DateTimeDisplay;
