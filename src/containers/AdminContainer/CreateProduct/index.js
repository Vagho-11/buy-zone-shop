import React, { useState, useEffect } from "react";
import { useFieldArray, useForm, Controller } from "react-hook-form";
import { axiosInstance } from "../../../libraries/axiosInstance";

import "./CreateProduct.css";

const CreateProduct = () => {
  const [image, setImage] = useState("");
  const { register, handleSubmit, formState, control, reset, watch } = useForm({
    mode: "all",
    defaultValues: {
      advantages: [{ advantage: "" }],
      comments: [
        {
          userName: "",
          text: "",
          date: "",
        },
      ],
    },
  });
  const images = watch("image");
  useEffect(() => {
    if (images) {
      const file = images[0];
      const reader = new FileReader();
      reader.onload = () => {
        setImage(reader.result.toString());
      };
      if (file) reader.readAsDataURL(file);
    }
  }, [images]);

  const { fields, append, remove } = useFieldArray({
    control,
    name: "advantages",
  });
  const {
    fields: commentsFields,
    append: commentsAppend,
    remove: commentsRemove,
  } = useFieldArray({ control, name: "comments" });
  const { isValid } = formState;
  const onSubmit = async (valuse) => {
    valuse.image = image;
    await axiosInstance.post(`Product/Create`, valuse);
    reset();
  };

  return (
    <div className="form_wrapper">
      <h1 className="form_title">Create Product</h1>
      <form className="Creat_form" onSubmit={handleSubmit(onSubmit)}>
        <label>Name</label>
        <input
          className="input_text"
          {...register("Name", { required: true })}
        />
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <label>New Price</label>
            <input
              type="number"
              className="input_number"
              {...register("NewPrice", { required: true })}
            />
          </div>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <label>Old Price</label>
            <input
              type="number"
              className="input_number"
              {...register("OldPrice", { required: true })}
            />
          </div>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <label>Percent </label>
            <input
              type="number"
              className="input_number"
              {...register("percent", { required: true })}
            />
          </div>
        </div>
        <label>Description</label>
        <input
          className="input_text"
          {...register("description", { required: true })}
        />
        <label>Video Url</label>
        <input
          className="input_text"
          {...register("videoUrl", { required: true })}
        />
        {
          <>
            <h2 style={{ padding: "10px 0" }}>Advantages</h2>
            {fields.map((item, index) => {
              return (
                <div className="advantage" key={item.id}>
                  <p>advantage</p>
                  <Controller
                    render={({ field }) => <input {...field} />}
                    name={`advantages[${index}].advantage`}
                    control={control}
                    rules={{ required: true }}
                  />
                  <button
                    className="del_button"
                    type="button"
                    onClick={() => remove(index)}
                  >
                    Delete
                  </button>
                </div>
              );
            })}
            <button
              className="add_button"
              type="button"
              onClick={() => {
                append();
              }}
            >
              Add advantage
            </button>
          </>
        }
        {
          <>
            <h2 style={{ padding: "10px 0" }}>Comments</h2>
            {commentsFields.map((item, index) => {
              return (
                <div className="advantage" key={item.id}>
                  <p>userName</p>
                  <input
                    className="input_text"
                    {...register(`comments.${index}.userName`, {
                      required: true,
                    })}
                  />
                  <p>date</p>
                  <input
                    className="input_text"
                    {...register(`comments.${index}.date`, { required: true })}
                  />
                  <p>text</p>
                  <Controller
                    render={({ field }) => <input {...field} />}
                    name={`comments[${index}].text`}
                    control={control}
                    rules={{ required: true }}
                  />

                  <button
                    className="del_button"
                    type="button"
                    onClick={() => commentsRemove(index)}
                  >
                    Delete
                  </button>
                </div>
              );
            })}
            <button
              className="add_button"
              type="button"
              onClick={() => {
                commentsAppend({
                  userName: "",
                  text: "",
                });
              }}
            >
              Add comment
            </button>
          </>
        }
        <div
          style={{ display: "flex", flexDirection: "column", margin: "10px 0" }}
        >
          <label className="upload_button" htmlFor="image">
            Upload image
          </label>
          <input
            id="image"
            style={{ display: "none" }}
            type="file"
            {...register("image", { required: true })}
          />
          {image ? <img src={image} width="450" /> : null}
        </div>

        <button className="prodCreateBtn" disabled={!isValid} type="submit">
          Create Prodcut
        </button>
      </form>
    </div>
  );
};
export default CreateProduct;
