import React from "react";
import { useCountdown } from "../../hooks/useCountDown";
import DateTimeDisplay from "./DateTimeDisplay";
import "./CountDown.css";

const CountdownTimer = ({ targetDate }) => {
  const [days, hours, minutes, seconds] = useCountdown(targetDate);

  if (days + hours + minutes + seconds <= 0) {
    return (
      <div className="expired-notice">
        <span>Expired!!!</span>
        <p>Please select a future date and time.</p>
      </div>
    );
  } else {
    return (
      <div className="show-counter">
        <div className="title">Limited Time Offer</div>
        <div className="time_block">
          <DateTimeDisplay value={days} type={"Days"} />
          <p className="dot">:</p>
          <DateTimeDisplay value={hours} type={"Hours"} />
          <p className="dot">:</p>
          <DateTimeDisplay value={minutes} type={"Mins"} />
          <p className="dot">:</p>
          <DateTimeDisplay value={seconds} type={"Seconds"} />
        </div>
      </div>
    );
  }
};
export default CountdownTimer;
