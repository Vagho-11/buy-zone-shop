import React from "react";
import { MainContainer, Admincontainer, HomeContainer } from "./containers";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./index.css";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<HomeContainer />} />
          <Route path="/Product/Detail/:id" element={<MainContainer />} />
          <Route path="/adminchik" element={<Admincontainer />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
