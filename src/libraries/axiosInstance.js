import axios from "axios";

const constants = {
  API_BASE_URL:
    process.env.REACT_APP_NODE_ENV === "production"
      ? "https://api.buyzone.life/api/"
      : "https://api.buyzone.life/api/",
};
const requestDefaultConfig = {
  baseURL: constants.API_BASE_URL,
};

export const axiosInstance = axios.create(requestDefaultConfig);
