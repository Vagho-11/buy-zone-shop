import React from "react";
import { ReactComponent as LogoWeb } from "../../icons/logo-Web.svg";

const Home = () => {
  return (
    <div className="parent">
      <div className="bg-logo">
        <LogoWeb />
        <LogoWeb style={{ transform: "rotate(45deg)" }} />
      </div>
    </div>
  );
};
export default Home;
