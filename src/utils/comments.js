export const comments = [
  {
    color: "#EE6393",
    id: 1,
    author: "Gayane",
    data: "12.02.22",
    comment:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
  },
  {
    color: "#F8BC01",
    id: 2,
    author: "Gayane",
    data: "12.02.22",
    comment:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
  },
  {
    color: "#817CFF",
    id: 3,
    author: "Gayane",
    data: "12.02.22",
    comment:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
  },
  {
    color: "#4BCBB1",
    id: 4,
    author: "Gayane",
    data: "12.02.22",
    comment:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
  },
  {
    color: "#EE6393",
    id: 5,
    author: "Gayane",
    data: "12.02.22",
    comment:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
  },
  {
    color: "#F8BC01",
    id: 6,
    author: "Gayane",
    data: "12.02.22",
    comment:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
  },
];
