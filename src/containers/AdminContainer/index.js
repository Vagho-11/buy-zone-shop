import React, { useState } from "react";
import { ReactComponent as LogoMobil } from "../../icons/logo-Mobil.svg";
import { ReactComponent as Product } from "../../icons/products.svg";
import { ReactComponent as AddProduct } from "../../icons/add_item.svg";
import CreateProduct from "./CreateProduct";
import Products from "./Products";
import classNames from "classnames";
import "./Admin.css";

const Admin = () => {
  const [activTab, setActiveTab] = useState(1);
  return (
    <>
      <div className="wrapper">
        <div className="sidebar">
          <div className="logo">
            <LogoMobil />
          </div>
          <div className="sidebar-wrapper">
            <ul className="nav">
              <li
                onClick={() => setActiveTab(1)}
                className={classNames("nav-item", {
                  ["active"]: activTab === 1,
                })}
              >
                <Product className="icon" />
                <p>Products</p>
              </li>
              <li
                onClick={() => setActiveTab(2)}
                className={classNames("nav-item", {
                  ["active"]: activTab === 2,
                })}
              >
                <AddProduct className="icon" />
                <p>Create Product</p>
              </li>
            </ul>
          </div>
        </div>
        <div className="admin_main">
          {activTab === 1 ? <Products /> : <CreateProduct />}
        </div>
      </div>
    </>
  );
};

export default Admin;
