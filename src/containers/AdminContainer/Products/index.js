import React, { useEffect, useState } from "react";
import { axiosInstance } from "../../../libraries/axiosInstance";
import "./Products.css";

const Products = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    axiosInstance.get(`Product/GetAll`).then(({ data }) => setData(data.data));
  }, []);
  const productsJsx = data?.map(({ id, name, photo }) => {
    return (
      <div className="card">
        <div className="card_photo">
          <img
            className="card_img"
            src={`https://api.buyzone.life/Files/${photo}`}
          />
        </div>
        <p className="card_name">
          Name:<span className="card_span">{name}</span>
        </p>
        <p className="card_id">
          Id:<span className="card_span">{id}</span>
        </p>
      </div>
    );
  });
  return <div className="card_wrapper">{productsJsx}</div>;
};
export default Products;
