import React, { useEffect, useState } from "react";
import { ReactComponent as LogoWeb } from "../../icons/logo-Web.svg";
import { ReactComponent as Off_49 } from "../../icons/49%OFF.svg";
import { ReactComponent as Off_50 } from "../../icons/50%OFF.svg";
import { ReactComponent as Off_51 } from "../../icons/51%OFF.svg";
import { ReactComponent as Off_52 } from "../../icons/52%OFF.svg";
import { ReactComponent as Off_53 } from "../../icons/53%OFF.svg";
import { ReactComponent as Author } from "../../icons/author.svg";
import { ReactComponent as Bus } from "../../icons/bus.svg";
import { ReactComponent as List } from "../../icons/list.svg";
import { ReactComponent as Phone } from "../../icons/phone.svg";
import { CountdownTimer } from "../../components/index";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";
import { axiosInstance } from "../../libraries/axiosInstance";

import "./Main.css";

const Main = () => {
  const [data, setData] = useState([]);
  const { register, handleSubmit, formState, reset } = useForm({ mode: "all" });
  const { isValid } = formState;
  const { id } = useParams();
  const THREE_DAYS_IN_MS = 3 * 24 * 60 * 60 * 1000;
  const NOW_IN_MS = new Date().getTime();
  const dateTimeAfterThreeDays = NOW_IN_MS + THREE_DAYS_IN_MS;

  const onSubmit = async ({ phoneNumber }) => {
    await axiosInstance.get(
      `Product/CreateOrder?productId=${id}&phoneNumber=${phoneNumber}`
    );
    reset();
  };

  const PercentRender = (type) => {
    switch (type) {
      case 49:
        return <Off_49 />;
      case 50:
        return <Off_50 />;
      case 51:
        return <Off_51 />;
      case 52:
        return <Off_52 />;
      case 53:
        return <Off_53 />;

      default:
        return <Off_50 />;
    }
  };
  const ColorRender = (index) => {
    switch (index) {
      case 1:
        return "#EE6393";
      case 2:
        return "#F8BC01";
      case 3:
        return "#817CFF";
      case 4:
        return "#4BCBB1";
      case 5:
        return "#EE6393";

      default:
        return "#817CFF";
    }
  };

  useEffect(() => {
    axiosInstance
      .get(`Product/Detail/${id}`)
      .then(({ data }) => setData(data.data));
  }, []);

  const commentsJsx = data?.comments?.map(({ userName, date, text }, index) => {
    return (
      <div
        className="comments_item"
        style={{ background: ColorRender(index) }}
        key={index}
      >
        <div className="author_block">
          <Author />
          <div style={{ marginLeft: "15px" }}>
            <p className="comments_author">{userName}hbced</p>
            <span className="comments_date">{date}hbdsh</span>
          </div>
        </div>
        <p className="comments_item_text">{text}</p>
      </div>
    );
  });
  const advantagesJsx = data?.advantages?.map(({ advantage }, index) => {
    return (
      <div key={index} className="advantages_item">
        <div className="circle">
          <div className="checkmark"></div>
        </div>
        <p className="advantages_item_text">{advantage}</p>
      </div>
    );
  });

  return (
    <>
      <div className="parent">
        <div className="bg-logo">
          <LogoWeb />
          {PercentRender(data.percent)}
        </div>
      </div>
      <div className="main">
        <img className="product_img" src={data?.photo} />
        <div className="price">
          <div className="old_price">
            <p className="old_price_text">Old price</p>
            <p className="old_price_num">
              <s>{data?.oldPrice}</s>
            </p>
          </div>
          <div className="new_price">
            <p className="new_price_text">New price.</p>
            <p className="new_price_num">{data?.newPrice}</p>
          </div>
        </div>
        <div className="btn_wrapper">
          <button className="btn">Order By Discount</button>
        </div>
        <div className="notification">
          <p>THERE ARE ONLY 10 LEFT</p>
        </div>
        <div className="count_timer">
          <CountdownTimer targetDate={dateTimeAfterThreeDays} />
        </div>
        <div className="advantages">
          <h4 className="advantages_title">Advantages</h4>
          {advantagesJsx}
        </div>
        <div className="description">
          <h3 className="description_title">Description</h3>
          <p className="description_text">{data?.description}</p>
        </div>
        <div className="video_content">
          <iframe
            className="video"
            src={data?.videoUrl}
            title="YouTube video player"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          ></iframe>
        </div>
        <div className="comments">
          <h3 className="comments_title">comments</h3>
          <div className="comments_content">{commentsJsx}</div>
          <div className="btn_block">
            <button className="comments_btn">Write a comment</button>
          </div>
        </div>
        <div className="order_block">
          <h3 className="order_block_title">How to order</h3>
          <div className="order_block_items">
            <div className="item" style={{ maxWidth: "332px", width: "100%" }}>
              <div className="item_one">Our managers contact you</div>
              <div className="arrow left"></div>
            </div>
            <List />
          </div>
          <div className="order_block_items">
            <div className="item" style={{ maxWidth: "352px", width: "100%" }}>
              <div className="item_two">Our managers contact you</div>
              <div className="arrow left"></div>
            </div>

            <Phone />
          </div>
          <div className="order_block_items">
            <div className="item" style={{ maxWidth: "382px", width: "100%" }}>
              <div className="item_tree">Our managers contact you</div>
              <div className="arrow left"></div>
            </div>
            <Bus />
          </div>
          <div className="notification">
            <p>Pay on the spot</p>
          </div>
          <form onSubmit={handleSubmit(onSubmit)} className="form">
            <div className="btn_wrapper">
              <input
                className="PhoneInput"
                placeholder="+374 | ** ** ** **"
                type="text"
                {...register("phoneNumber", {
                  required: true,
                  pattern: {
                    value:
                      /^(\+374|374|0)?[\s\-]?\(?[0-9]{2}\)?[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/i,
                  },
                })}
              />
              <button type="submit" disabled={!isValid} className="btn">
                Order By Discount
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default Main;
